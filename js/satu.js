var nama = ['Gottfried']
var usia = [25]

// Gabungkan nama dengan usia
var array1 = nama.concat(usia)
console.log(array1.toString()) // tampilkan sebagai string

// Sisipkan tahun setelah nama, sebelum usia
array1.splice(1, 0, 1990)
console.log(array1.toString())

// Cetak setiap elemen ke console dengan for loop

function cetakElemen(ary) {
    for (var i = 0; i < ary.length; i++) {
        console.log(ary[i])
    }
}

cetakElemen(array1)