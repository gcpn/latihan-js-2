function UbahAngka(n) {
    var angkaInput = document.getElementById('angkaInput')
    var angka = parseInt(angkaInput.value)
    angka += n
    angkaInput.value = angka
}

document.getElementById('kurang10').onclick = function() { UbahAngka(-10) }
document.getElementById('kurang1').onclick = function() { UbahAngka(-1) }
document.getElementById('tambah1').onclick = function() { UbahAngka(1) }
document.getElementById('tambah10').onclick = function() { UbahAngka(10) }